import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import domain.Pizza;
import domain.SliceResult;
import io.FileReader;
import io.FileReaderImpl;
import io.FileWriter;
import io.FileWriterImpl;
import slicer.ExampleSlicer;
import slicer.PizzaSlicer;

public class Main {

    public static void main(String[] args) throws IOException {

//        if (args.length < 1) {
//            System.out.println("Parameter 1 must be the input file path.");
//            System.exit(1);
//        }

//        String inputPathStr = args[0];
        String inputPathStr = "small.in";

        FileReader fileReader = new FileReaderImpl();

        Pizza pizza = fileReader.readFile(inputPathStr);

        PizzaSlicer downSlicerImpl = new ExampleSlicer();

        SliceResult result = downSlicerImpl.slice(pizza);

        FileWriter fileWriter = new FileWriterImpl();

        Path inputPath = Paths.get(inputPathStr);
        Path inputPathFileName = inputPath.getFileName();
        fileWriter.writeToFile("./" + inputPathFileName + "-result.txt", result);
    }
}
