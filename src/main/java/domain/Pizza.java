package domain;

public interface Pizza {

    int getNumRows();

    int getNumColumns();

    int getMinToppings();

    int getMaxToppings();

    Topping get(int row, int column);

    void print();
}
