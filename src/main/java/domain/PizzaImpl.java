package domain;

import java.util.Map;

public class PizzaImpl implements Pizza {

    // row position -> column position -> Topping
    private Map<Integer, Map<Integer, Topping>> coordToTopping;

    private int numRows;
    private int numColumns;
    private int minToppings;
    private int maxToppings;

    public PizzaImpl(Map<Integer, Map<Integer, Topping>> coordToTopping, int numRows, int numColumns, int minToppings, int maxToppings) {
        this.coordToTopping = coordToTopping;
        this.numRows = numRows;
        this.numColumns = numColumns;
        this.minToppings = minToppings;
        this.maxToppings = maxToppings;
    }

    @Override
    public Topping get(int row, int column) {
        return coordToTopping.get(row).get(column);
    }

    @Override
    public int getMaxToppings() {
        return maxToppings;
    }

    @Override
    public int getMinToppings() {
        return minToppings;
    }

    @Override
    public int getNumColumns() {
        return numColumns;
    }

    @Override
    public int getNumRows() {
        return numRows;
    }

    @Override
    public void print() {
        System.out.println("Min toppings: " + minToppings);
        System.out.println("Max toppings: " + maxToppings);

        for (int r = 0; r < numRows; r++) {
            for (int c = 0; c < numColumns; c++) {
                System.out.print(get(r, c));
            }

            System.out.println("");
        }
    }
}
