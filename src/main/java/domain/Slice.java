package domain;

public interface Slice {

    Integer getTopLeftRow();

    Integer getTopLeftColumn();

    Integer getBottomRightRow();

    Integer getBottomRightColumn();

    int getNumberOfToppings();

    boolean isSatisfied(Pizza pizza, int minToppings, int maxToppings);

    Slice doCopy();

    void setBottomRightRow(Integer bottomRightRow);

    void setTopLeftRow(Integer topLeftRow);

    void setTopLeftColumn(Integer topLeftColumn);
}
