package domain;

public class SliceImpl implements Slice {

    private Integer topLeftRow;
    private Integer topLeftColumn;

    private Integer bottomRightRow;
    private Integer bottomRightColumn;

    public SliceImpl(Integer topLeftRow, Integer topLeftColumn, Integer bottomRightRow, Integer bottomRightColumn) {
        this.topLeftRow = topLeftRow;
        this.topLeftColumn = topLeftColumn;
        this.bottomRightRow = bottomRightRow;
        this.bottomRightColumn = bottomRightColumn;
    }

    @Override
    public Slice doCopy() {
        return new SliceImpl(topLeftRow, topLeftColumn, bottomRightRow, bottomRightColumn);
    }

    @Override
    public boolean isSatisfied(Pizza pizza, int minToppings, int maxToppings) {
        int tomatoCount = 0;
        int mushroomCount = 0;

        for (int r = topLeftRow; r <= bottomRightRow; r++) {
            for (int c = topLeftColumn; c <= bottomRightColumn; c++) {
                Topping topping = pizza.get(r, c);

                switch (topping) {
                    case MUSHROOM:
                        mushroomCount++;
                        break;
                    case TOMATO:
                        tomatoCount++;
                        break;
                    default:
                        throw new RuntimeException("Unexpected topping");
                }
            }
        }

        boolean enoughMushrooms = mushroomCount > minToppings;
        boolean enoughTomatoes = tomatoCount > minToppings;

        boolean minSatisfied = enoughMushrooms && enoughTomatoes;
        boolean maxSatisfied = tomatoCount + mushroomCount <= maxToppings;

        return minSatisfied && maxSatisfied;
    }

    @Override
    public Integer getTopLeftRow() {
        return topLeftRow;
    }

    @Override
    public void setTopLeftRow(Integer topLeftRow) {
        this.topLeftRow = topLeftRow;
    }

    @Override
    public Integer getTopLeftColumn() {
        return topLeftColumn;
    }

    @Override
    public void setTopLeftColumn(Integer topLeftColumn) {
        this.topLeftColumn = topLeftColumn;
    }

    @Override
    public Integer getBottomRightRow() {
        return bottomRightRow;
    }

    @Override
    public void setBottomRightRow(Integer bottomRightRow) {
        this.bottomRightRow = bottomRightRow;
    }

    @Override
    public Integer getBottomRightColumn() {
        return bottomRightColumn;
    }

    public void setBottomRightColumn(Integer bottomRightColumn) {
        this.bottomRightColumn = bottomRightColumn;
    }

    @Override
    public String toString() {
        return "(" + topLeftRow + ", " + topLeftColumn
                + ") -> (" + bottomRightRow + ", " + bottomRightColumn + ")";
    }

    @Override
    public int getNumberOfToppings() {
        int a = bottomRightRow - topLeftRow + 1;
        int b = bottomRightColumn - topLeftColumn + 1;

        return a * b;
    }
}
