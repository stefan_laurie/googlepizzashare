package domain;

import java.util.List;

public interface SliceResult {

    List<Slice> getSlices();

    void print();
}
