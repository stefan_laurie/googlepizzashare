package domain;

import java.util.List;

public class SliceResultImpl implements SliceResult {

    private List<Slice> slices;

    public SliceResultImpl(List<Slice> slices) {
        this.slices = slices;
    }

    @Override
    public List<Slice> getSlices() {
        return slices;
    }

    @Override
    public void print() {
        int count = 0;
        for (Slice slice : slices) {
            System.out.println("---");
            System.out.println(count + "| " + slice);
            System.out.println("---");
            count++;
        }
    }
}
