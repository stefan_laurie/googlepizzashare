package domain;

public enum Topping {
    MUSHROOM('M'), TOMATO('T');

    char asChar;

    Topping(char asChar) {
        this.asChar = asChar;
    }

    public static Topping fromChar(char c) {
        Topping[] values = Topping.values();
        for (Topping topping : values) {
            if (topping.asChar == c) {
                return topping;
            }
        }

        throw new RuntimeException("Unexpected char! " + c);
    }


    @Override
    public String toString() {
        return asChar + "";
    }
}
