package io;

import java.io.IOException;

import domain.Pizza;

public interface FileReader {

    Pizza readFile(String path) throws IOException;
}
