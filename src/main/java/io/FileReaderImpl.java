package io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import domain.Pizza;
import domain.PizzaImpl;
import domain.Topping;

public class FileReaderImpl implements FileReader {

    @Override
    public Pizza readFile(String pathStr) throws IOException {
        Path path = Paths.get(pathStr);

        Pizza pizza;

        try (Stream<String> stream = Files.lines(path)) {

            String[] lines = stream.toArray(String[]::new);

            Map<Integer, Map<Integer, Topping>> pizzaCoords = new HashMap<>();

            int row = -1;

            int numRows = 0;
            int numColumns = 0;
            int minToppings = 0;
            int maxToppings = 0;

            for (String line : lines) {

                //extract from first row
                if (row == -1) {
                    String[] split = line.split(" ");
                    numRows = Integer.valueOf(split[0]);
                    numColumns = Integer.valueOf(split[1]);
                    minToppings = Integer.valueOf(split[2]);
                    maxToppings = Integer.valueOf(split[3]);

                    row++;
                    continue;
                }

                int column = 0;

                for (char c : line.toCharArray()) {
                    Topping topping = Topping.fromChar(c);

                    Map<Integer, Topping> columnToToppingMap = pizzaCoords.get(row);
                    if (columnToToppingMap == null) {
                        columnToToppingMap = new HashMap<>();
                        pizzaCoords.put(row, columnToToppingMap);
                    }

                    columnToToppingMap.put(column, topping);

                    column++;
                }


                row++;
            }

            pizza = new PizzaImpl(pizzaCoords, numRows, numColumns, minToppings, maxToppings);

        } catch (IOException ex) {
            ex.printStackTrace();
            throw ex;
        }

        return pizza;
    }
}
