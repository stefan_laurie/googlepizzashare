package io;

import java.io.IOException;

import domain.SliceResult;

public interface FileWriter {

    void writeToFile(String path, SliceResult sliceResult) throws IOException;
}
