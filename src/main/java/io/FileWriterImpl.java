package io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import domain.Slice;
import domain.SliceResult;

public class FileWriterImpl implements FileWriter {

    @Override
    public void writeToFile(String pathStr, SliceResult sliceResult) throws IOException {
        Path path = Paths.get(pathStr);

        try (BufferedWriter writer = Files.newBufferedWriter(path, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE)) {
            List<Slice> allSlices = sliceResult.getSlices();
            writer.write(allSlices.size() + "");

            for (Slice slice : allSlices) {
                writer.write(System.lineSeparator());

                int r1 = slice.getTopLeftRow();
                int c1 = slice.getTopLeftColumn();

                int r2 = slice.getBottomRightRow();
                int c2 = slice.getBottomRightColumn();

                writer.write(r1 + " " + c1 + " " + r2 + " " + c2);
            }
        }
    }
}
