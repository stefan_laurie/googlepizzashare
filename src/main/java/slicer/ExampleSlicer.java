package slicer;

import java.util.ArrayList;
import java.util.List;

import domain.Pizza;
import domain.Slice;
import domain.SliceImpl;
import domain.SliceResult;
import domain.SliceResultImpl;

/**
 * Just makes one slice
 */
public class ExampleSlicer implements PizzaSlicer {

    @Override
    public SliceResult slice(Pizza pizza) {

        SliceImpl slice = new SliceImpl(0, 0, 1, 1);

        List<Slice> allSlices = new ArrayList<>();

        allSlices.add(slice);

        return new SliceResultImpl(allSlices);
    }
}
