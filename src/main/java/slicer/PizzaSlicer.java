package slicer;

import domain.Pizza;
import domain.SliceResult;

public interface PizzaSlicer {

    SliceResult slice(Pizza pizza);
}
