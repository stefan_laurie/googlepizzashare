import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import domain.Pizza;
import domain.Slice;
import domain.SliceResult;
import domain.Topping;
import io.FileReader;
import io.FileReaderImpl;
import org.junit.Test;
import slicer.ExampleSlicer;
import slicer.PizzaSlicer;
import static org.junit.Assert.fail;

public class SlicerTest {

    // to PizzaSlicer to test
    private PizzaSlicer pizzaSlicerToUse = new ExampleSlicer();

    @Test
    public void testExample() throws Exception {
        testAndGetArea("example.in");
    }

    @Test
    public void testSmall() throws Exception {
        testAndGetArea("small.in");
    }

    @Test
    public void testMedium() throws Exception {
        testAndGetArea("medium.in");
    }

    @Test
    public void testBig() throws Exception {
        testAndGetArea("big.in");
    }

    @Test
    public void testScore() throws Exception {
        int score = 0;

        score += testAndGetArea("small.in");
        score += testAndGetArea("medium.in");
        score += testAndGetArea("big.in");

        int totalArea = 1000000; // big
        totalArea += 50000; // medium
        totalArea += 42; // small

        String formattedScore = NumberFormat.getNumberInstance().format(score);
        System.out.println("Total score: " + formattedScore); //884,871
        System.out.println("%: " + score / (double) totalArea * 100);
    }

    private int testAndGetArea(String inputPath) throws Exception {
        FileReader fileReader = new FileReaderImpl();

        Pizza pizza = fileReader.readFile(inputPath);

        SliceResult result = pizzaSlicerToUse.slice(pizza);

//        result.print();

        System.out.println("Number of slices: " + result.getSlices().size());

        int slicesArea = assertSlicesAreSatisfied(pizza, result);

        assertSlicesDoNotOverlap(result);

        return slicesArea;
    }

    private int assertSlicesAreSatisfied(Pizza pizza, SliceResult sliceResult) {
        int minToppings = pizza.getMinToppings();
        int maxToppings = pizza.getMaxToppings();

        int slicesArea = 0;

        List<Slice> slices = sliceResult.getSlices();
        int counter = 0;
        for (Slice slice : slices) {
            int tomatoCount = 0;
            int mushroomCount = 0;

            Integer topLeftRow = slice.getTopLeftRow();
            Integer topLeftColumn = slice.getTopLeftColumn();
            Integer bottomRightRow = slice.getBottomRightRow();
            Integer bottomRightColumn = slice.getBottomRightColumn();

            for (int r = topLeftRow; r <= bottomRightRow; r++) {
                for (int c = topLeftColumn; c <= bottomRightColumn; c++) {
                    Topping topping = pizza.get(r, c);

                    switch (topping) {
                        case MUSHROOM:
                            mushroomCount++;
                            break;
                        case TOMATO:
                            tomatoCount++;
                            break;
                        default:
                            throw new RuntimeException("Unexpected topping");
                    }
                }
            }

            if (mushroomCount < minToppings) {
                fail("not enough mushrooms! slice " + counter);
            }

            if (tomatoCount < minToppings) {
                fail("not enough tomatoes! slice " + counter);
            }

            int area = tomatoCount + mushroomCount;
            if (area > maxToppings) {
                fail("too many toppings! slice " + counter);
            }

            slicesArea += area;

            counter++;
        }

        int pizzaArea = pizza.getNumRows() * pizza.getNumColumns();
        System.out.println("Total Area: " + slicesArea / (double) pizzaArea * 100 + "%");

        return slicesArea;
    }

    private void assertSlicesDoNotOverlap(SliceResult sliceResult) {
        // row -> column -> Exists
        Map<Integer, Map<Integer, Integer>> visitedCoords = new HashMap<>();

        List<Slice> slices = sliceResult.getSlices();
        int counter = 0;
        for (Slice slice : slices) {

            for (int r = slice.getTopLeftRow(); r <= slice.getBottomRightRow(); r++) {
                for (int c = slice.getTopLeftColumn(); c <= slice.getBottomRightColumn(); c++) {
                    Map<Integer, Integer> colToExistsMap = visitedCoords.get(r);
                    if (colToExistsMap == null) {
                        colToExistsMap = new HashMap<>();
                        visitedCoords.put(r, colToExistsMap);
                    }
                    Integer existingCoord = colToExistsMap.get(c);
                    if (existingCoord != null) {
                        fail("slices overlap! slices: " + existingCoord + " and " + counter);
                    } else {
                        colToExistsMap.put(c, counter);
                    }
                }
            }

            counter++;
        }
    }
}
